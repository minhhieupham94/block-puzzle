using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class MatchScoreData
{
    public int MathchBlock;
    public int Point;
}
[CreateAssetMenu(fileName = "new MatchScoreDesign", menuName = "Block_Pizzle/MatchScoreDesign")]
public class MatchScoreDesign : ScriptableObject
{
    public int MaxMatch;
    public MatchScoreData[] List;
}
