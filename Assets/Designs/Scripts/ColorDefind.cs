using System;
using UnityEngine;

[Serializable]
public class ColorTypeBlock
{
    public int TypeBlock;
    public Color Color;
}

[CreateAssetMenu(fileName = "new ColorDefind",menuName = "Block_Pizzle/Color Defind")]

public class ColorDefind : ScriptableObject
{
    public ColorTypeBlock[] ListColor;
}
