using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class TimeStreakData
{
    public int Streak;
    public float Time;
    public int Point;

    public TimeStreakData Clone()
    {
        return new TimeStreakData()
        {
            Streak = Streak,
            Time = Time,
            Point = Point
        };
    }
}
[CreateAssetMenu(fileName = "new TimeStreak", menuName = "Block_Pizzle/TimeStreak")]
public class TimeStreak : ScriptableObject
{
    public int MaxStreak;
    public TimeStreakData[] List;
}
