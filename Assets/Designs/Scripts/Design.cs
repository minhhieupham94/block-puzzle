using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName = "new Design",menuName = "Block_Pizzle/Design")]

public class Design : ScriptableObject
{
    public int Matchs;
    public int PointTarget;
    public int Shape1;
    public int Shape2;
    public int Shape3;
    public int Shape4;
    public int DuplicateShape;
    public int DuplicateItem;
    public int Max;
    public int Duplicate;
    public int Type1;
    public int Type2;
    public int Type3;

    public Design Clone()
    {
        return new Design()
        {
            Matchs = Matchs,
            PointTarget = PointTarget,
            Shape1 = Shape1,
            Shape2 = Shape2,
            Shape3 = Shape3,
            Shape4 = Shape4,
            DuplicateShape = DuplicateShape,
            DuplicateItem = DuplicateItem,
            Max = Max,
            Duplicate = Duplicate,
            Type1 = Type1,
            Type2 = Type2,
            Type3 = Type3
        };
    }
}
