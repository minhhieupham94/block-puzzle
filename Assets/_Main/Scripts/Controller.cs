﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller
{
    public static Controller InStance;
    private int curDificultlevel;
    private Design _dataDesign;
    private int beginDifficultLevel;

    public Controller()
    {
        InStance = this;
    }

    public List<int> GetListTypeShape()
    {
        Dictionary<int,int> _data = new Dictionary<int, int>();  //type-amount
        List<int> _res = new List<int>();
        for (int i = 0; i < BoardManager.BLOCKS_AMOUNT; i++)
        {
            var _type = GetTypeShape();
            while (_data.ContainsKey(_type) && _data[_type] >= _dataDesign.DuplicateShape)
            {
                _type = GetTypeShape();
            }

            if(!_data.ContainsKey(_type))
            {
                _data[_type] = 0;
            }
            _data[_type]++;
            _res.Add(_type);
        }
        return _res;
    }

    public int GetBeginDifficultLevel()
    {
        int _match = GameSaveData.Instance.GetMatch();
        beginDifficultLevel = DesignManager.Instance.GetDifficultFromMatch(_match);
        return beginDifficultLevel;
    }

    public int GetCurrentDifficultLevel()
    {
        curDificultlevel = 0;
        // lấy độ khó qua match và score, cái nào cao hơn thì lấy

        int _core = GameManager.ins.Score;
        int _difficultPoint = DesignManager.Instance.GetDifficultFromPoint(_core);

        if (beginDifficultLevel >= _difficultPoint)
        {
            curDificultlevel = beginDifficultLevel;
        }
        else
        {
            curDificultlevel = _difficultPoint;
        }
        return curDificultlevel;
    }

    public int GetTypeShape()
    {
        Debug.Log("GetTypeShape");
        
        _dataDesign = DesignManager.Instance.GetDesign(GetCurrentDifficultLevel()).Data;

        int _random = Random.Range(0, 101);

        if(_random <= _dataDesign.Shape1)
        {
            return 1;
        }
        if (_random <= _dataDesign.Shape2)
        {
            return 2;
        }
        if (_random <= _dataDesign.Shape3)
        {
            return 3;
        }
        return 4;
    }

    public List<BlockTileData> GetRandomBlock(int _blockAmount)
    {
        Dictionary<int, List<BlockTileData>> _data = new Dictionary<int, List<BlockTileData>>();
        List<BlockTileData> _res = new List<BlockTileData>();
        for (int i = 0; i < _blockAmount; i++)
        {
            int _typeRandom = Random.Range(0, BoardManager.BLOCK_TYPE_AMOUNT);

            while(_data.ContainsKey(_typeRandom) && _data[_typeRandom].Count >= _dataDesign.DuplicateItem)
            {
                _typeRandom = Random.Range(0, BoardManager.BLOCK_TYPE_AMOUNT);
            }

            if (!_data.ContainsKey(_typeRandom))
            {
                _data[_typeRandom] = new List<BlockTileData>();
            }
            var _dt = new BlockTileData() { Type = _typeRandom };
            _data[_typeRandom].Add(_dt);
            _res.Add(_dt);
        }

        return _res;
    }

    public Color GetColorTypeBlock(int _typeBlock)
    {
        return DesignManager.Instance.GetColorTypeBlock(_typeBlock);
    }
}

