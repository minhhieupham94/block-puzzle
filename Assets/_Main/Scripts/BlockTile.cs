﻿using System;
using UnityEngine;

[Serializable]
public class BlockTileData
{
    public int Type;
}
public class BlockTile : MonoBehaviour
{
    public Color defaultColor { get; set; }
    [SerializeField]private BlockTileData data;
    private BlockDestroyAnimation animDestroy;
    private BlockFallAnimation animFall;
    private SpriteRenderer spriteRenderer;
    private Vector2Int coordinate;

    public Vector2Int Coordinate => coordinate;

    public void SetCoordinate(Vector2Int _coor)
    {
        coordinate = _coor;
    }
    public BlockDestroyAnimation AnimDestroy
    { 
        get
        {
            if (animDestroy == null)
            {
                animDestroy = GetComponent<BlockDestroyAnimation>();
            }
            return animDestroy;
        }
    }
    public BlockFallAnimation AnimFall
    {
        get
        {
            if (animFall == null)
            {
                animFall = GetComponent<BlockFallAnimation>();
            }
            return animFall;
        }
    }

    public SpriteRenderer SpriteRenderer
    {
        get
        {
            if (spriteRenderer == null)
            {
                spriteRenderer = GetComponent<SpriteRenderer>();
            }
            return spriteRenderer;
        }
    }

    public void Fade(float d, Color c)
    {
        BlockFadeAnimation anim = GetComponent<BlockFadeAnimation>();
        anim.enabled = true;
        anim.SetAnimation(d, c);
    }

    public void Fall(float d, BlockFallAnimation.Direction dir)
    {
        AnimFall.enabled = true;
        AnimFall.SetAnimation(d, dir);
    }

    public void Destroy(float d)
    {
        AnimDestroy.enabled = true;
        AnimDestroy.SetAnimation(d);
    }

    public void SetColor(Color _color)
    {
        Debug.Log($"SetColor {data.Type} || {_color}");
        Color _cl = defaultColor;
        _cl.a = _color.a;
        SpriteRenderer.color = _cl;
    }

    internal void SetData(BlockTileData _data)
    {
        if(_data != null)
        {
            data = _data;
            var _color = Controller.InStance.GetColorTypeBlock(data.Type);
            defaultColor = _color;
            SetColor(_color);
        }
    }

    public void Show()
    {
        Color _c = defaultColor;
        _c.a = 1;
        SpriteRenderer.color = _c;
    }

    public int GetTypeBlock()
    {
        if(data != null)
        {
            return data.Type;
        }
        return 0;
    }
    public bool IsDestroy { get; private set; }

    private void OnDestroy()
    {
        IsDestroy = true;
    }
}
