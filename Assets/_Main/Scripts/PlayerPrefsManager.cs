using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPrefsKey
{
    public const string FIRST_BEATON_SCORE = "firstBeatenScore";
    public const string CONTINUE_GAME = "continueGame";
    public const string MUSIC = "musicMute";
    public const string SOUND = "soundMute";
    public const string MUSIC_VOL = "musicVolume";
    public const string SOUND_VOL= "soundVolume";
    public static string Block(int v)
    {
        return $"{v} block";
    }
}
public static class PlayerPrefsManager
{
    public static void SetInt(string key, int value)
    {
        PlayerPrefs.SetInt(key, value);
    }
    public static int GetInt(string key)
    {
        return PlayerPrefs.GetInt(key);
    }

    public static void DeleteAll()
    {
        PlayerPrefs.DeleteAll();
    }

    public static bool HasKey(string v)
    {
        return PlayerPrefs.HasKey(v);
    }
    public static bool GetBool(string k)
    {
        if(HasKey(k))
        {
            if (PlayerPrefs.GetInt(k) == 1)
            {
                return true;
            }
        }
        return false;
    }
    public static void SetBool(string k, bool v)
    {
        if (!v)
            PlayerPrefs.SetInt(k, 0);
        else
            PlayerPrefs.SetInt(k, 1);
    }

    public static void SetFloat(string k,float v)
    {
        PlayerPrefs.SetFloat(k, v);
    }
    public static float GetFloat(string k)
    {
        if(HasKey(k))
        {
            return PlayerPrefs.GetFloat(k);
        }
        return 0;
    }

    public static float[] GetFloatArray(string k, int s)
    {
        float[] arr = new float[s];

        if (!HasKey(k))
        {
            //Debug.LogError("The float array does not exist!");
            return null;
        }

        for (int i = 0; i < s; i++)
            arr[i] = GetFloat(i + k);

        return arr;
    }
    public static void SetFloatArray(string k, float[] arr)
    {
        PlayerPrefs.SetFloat(k, 0);

        for (int i = 0; i < arr.Length; i++)
            PlayerPrefs.SetFloat(i + k, arr[i]);
    }
}
