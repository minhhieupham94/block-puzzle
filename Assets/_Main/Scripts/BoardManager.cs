﻿using UnityEngine;
using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

[Serializable]
public class BlockDatas
{
    public int TypeShape;
    public Block[] Blocks;
}

public class BoardManager : MonoBehaviour
{
    public static BoardManager Instance;

    public const int BOARD_SIZE = 10;
    public const int BLOCKS_AMOUNT = 3;
    public const int BLOCK_PREFABS_AMOUNT = 18;
    public const int BLOCK_TYPE_AMOUNT = 8;

    public GameObject boardTilePrefab;
    public GameObject blockTilePrefab;
    public BlockDatas[] blockPrefabs;
    public Transform gameTransform;
    public Transform boardTransform;
    public Color boardColor;
    public Color highlightColor;

    [HideInInspector]
    public Vector3 boardTileScale;
    [HideInInspector]
    public Vector3 scaledBlockTileScale;
    [HideInInspector]
    public SpriteRenderer[,] boardTiles = new SpriteRenderer[BOARD_SIZE, BOARD_SIZE];
    [HideInInspector]
    public BlockTile[,] boardBlocks = new BlockTile[BOARD_SIZE, BOARD_SIZE];
    [HideInInspector]
    public Block[] blocks = new Block[BLOCKS_AMOUNT];
    private int blockCounter;
    private List<BlockTile> listConnectBlock;
    private Dictionary<int, Block> blockWaiting = new Dictionary<int, Block>();

    public bool IsInRange(Vector2 o, Vector2 e)
    {
        return o.x >= -0.5f && e.x <= BOARD_SIZE - 0.5f &&
               o.y >= -0.5f && e.y <= BOARD_SIZE - 0.5f;
    }

    public bool IsEmpty(Block b, Vector2 o)
    {
        int _childCount = b.transform.childCount;
        for (int i = 0; i < b.structure.Length; i++)
        {
            if(i >= _childCount)
            {
                break;
            }
            if (b.transform.GetChild(i).name == "Block tile")
            {
                Vector2Int coords = b.structure[i];
                if((int)o.x + coords.x < 0 || (int)o.x + coords.x >= BOARD_SIZE || (int)o.y + coords.y < 0 || (int)o.y + coords.y > BOARD_SIZE)
                {
                    return false;
                }
                if (boardBlocks[(int)o.x + coords.x, (int)o.y + coords.y])
                    return false;
            }

        }

        return true;
    }


    public static int Rand(int min, int max)
    {
        return (int)UnityEngine.Random.Range(min, max - 0.000001f);
    }

    public BlockTile SpawnBlockTile(int x, int y)
    {
        boardBlocks[x, y] = Instantiate(blockTilePrefab, boardTransform).GetComponent<BlockTile>();

        Vector3 pos = new Vector3(x, y, -1);
        boardBlocks[x, y].transform.position = pos;
        boardBlocks[x, y].transform.localScale = boardTileScale;

        return boardBlocks[x, y];
    }

    public Block SpawnBlock(int i, int _typeShape)
    {
        Debug.Log("Spawn block!!!");
        Block b = null;
        var _typeData = blockPrefabs.FirstOrDefault((v) => v.TypeShape == _typeShape);
        if(_typeData != null)
        {
            int _random = Rand(0, _typeData.Blocks.Length);
            b = Instantiate(_typeData.Blocks[_random], gameTransform);
            b.Initialized(new BlockData() { ListData = Controller.InStance.GetRandomBlock(_typeShape) });
            b.SetBasePosition(i);
        }
        return b;
    }

    private Vector3 RandomRotation()
    {
        int _r = UnityEngine.Random.Range(0, 4);
        return new Vector3(0, 0, _r * 90);
    }

    public int GetEmptyFieldsAmount()
    {
        int x = 0;

        foreach (BlockTile b in boardBlocks)
            if (!b)
                x++;

        return x;
    }

    public void MoveBlocks(int i)
    {
        Debug.Log($"MoveBlocks {i}");
        blockCounter++;
        if(blockCounter >= BLOCKS_AMOUNT)
        {
            var listData = Controller.InStance.GetListTypeShape();
            for (int j = 0; j < BLOCKS_AMOUNT; j++)
            {
                var _block = SpawnBlock(j, listData[j]);
                blocks[j] = _block;
                blocks[j].SetBasePosition(j, false);
                blocks[j].Move(0.2f, blocks[j].basePosition);
                blockCounter = 0;
            }
        }
        AddBlockWaiting(blocks);
    }

    public void CheckSpace(bool oa)
    {
        int count = 0;
        for (int i = 0; i < BLOCKS_AMOUNT; i++)
        {
            if (CheckBlock(i))
            {
                blocks[i].movable = true;
                blocks[i].Show();
            }
            else
            {
                blocks[i].movable = false;
                count++;

                Color c = blocks[i].GetColor();
                c.a = 0.5f;
                blocks[i].ChangeColor(c);
            }

            if (oa && count == BLOCKS_AMOUNT)
                GameManager.ins.RestartGame();
            else if (count == BLOCKS_AMOUNT)
                StartCoroutine(GameManager.ins.WaitForFade());
        }
    }

    public void CheckBoard(bool onAwake = false)
    {
        Debug.Log($"CheckBoard Point");
        DestroyManager.ins.SetDestroy();
        CheckConectedBlock(false);

        if (DestroyManager.ins.destroyedLines > 0)
            StartCoroutine(DestroyManager.ins.DestroyAllBlocks(listConnectBlock));
        else
            CheckSpace(onAwake);
    }

    public void HighlightBlocks()
    {
        Block db = InputManager.ins.draggedBlock;
        Vector2Int c = db.GetFirstCoords();
        CheckConectedBlock(true);
    }

    private void Awake()
	{
        if (!Instance)
            Instance = this;

    }

    public void Initialized()
    {
        boardTileScale = GameScaler.GetBoardTileScale();
        scaledBlockTileScale = GameScaler.GetScaledBlockTileScale();

        CreateBoard();
    }

    private void CreateBoard()
    {
        Vector3 scale = GameScaler.GetBoardTileScale();

        for (int y = 0; y < BOARD_SIZE; y++)
        {
            for (int x = 0; x < BOARD_SIZE; x++)
            {
                Transform t = Instantiate(boardTilePrefab, boardTransform).transform;
                t.position = new Vector3(x, y, 0);
                t.localScale = scale;
                boardTiles[x, y] = t.GetComponent<SpriteRenderer>();
            }
        }

        List<int> _listTypeShape = Controller.InStance.GetListTypeShape();
        for (int i = 0; i < BLOCKS_AMOUNT; i++)
        {
            if (!PlayerPrefsManager.HasKey(PlayerPrefsKey.Block(i)))
            {
                var _block = SpawnBlock(i, _listTypeShape[i]);
                blocks[i] = _block;
            }
        }
        AddBlockWaiting(blocks);
    }

    public void AddBlockWaiting(Block[] blocks)
    {
        if(blocks != null)
        {
            int i = 0;
            foreach (var b in blocks)
            {
                blockWaiting[i] = b;
                    i++;
            }
        }
    }

    public void RotateBlock(int i)
    {
        if(blockWaiting != null && blockWaiting.ContainsKey(i) && !blockWaiting[i].IsMoveEnd)
        {
            if(blockWaiting[i].size != Vector2.one)
            {
                blockWaiting[i].Rotate();
                GameManager.ins.UpdateRotate(blockWaiting[i].IsRotate);
            }
        }
    }

    private void CheckHLine(int y, bool h = false)
    {
        if (h)
        {
            BlockTile[,] b = new BlockTile[BOARD_SIZE, BOARD_SIZE];
            Array.Copy(boardBlocks, b, boardBlocks.Length);

            Block db = InputManager.ins.draggedBlock;
            Vector2Int c = db.GetFirstCoords();
            for (int i = 0; i < db.structure.Length; i++)
            {
                if (db.transform.GetChild(i).name == "Block tile")
                {
                    BlockTile bt = db.transform.GetChild(i).GetComponent<BlockTile>();
                    b[c.x + db.structure[i].x, c.y + db.structure[i].y] = bt;
                }
            }


            
            for (int x = 0; x < BOARD_SIZE; x++)
                if (!b[x, y])
                    return;
            
            for (int x = 0; x < BOARD_SIZE; x++)
                if (boardBlocks[x, y])
                    boardBlocks[x, y].Fade(0.2f, db.defaultColor);
        }
        else
        {
            for (int x = 0; x < BOARD_SIZE; x++)
                if (!boardBlocks[x, y])
                    return;

            DestroyManager.ins.PrepareToDestroy(y, false);
        }
    }

    private void CheckVLine(int x, bool h = false)
    {
        if (h)
        {
            BlockTile[,] b = new BlockTile[BOARD_SIZE, BOARD_SIZE];
            Array.Copy(boardBlocks, b, boardBlocks.Length);

            Block db = InputManager.ins.draggedBlock;
            Vector2Int c = db.GetFirstCoords();
            for (int i = 0; i < db.structure.Length; i++)
            {
                if (db.transform.GetChild(i).name == "Block tile")
                {
                    BlockTile bt = db.transform.GetChild(i).GetComponent<BlockTile>();
                    Vector2Int _coor = new Vector2Int(c.x + db.structure[i].x, c.y + db.structure[i].y);
                    bt.SetCoordinate(_coor);
                    b[_coor.x, _coor.y] = bt;
                }
            }

            for (int y = 0; y < BOARD_SIZE; y++)
                if (!b[x, y])
                    return;

            for (int y = 0; y < BOARD_SIZE; y++)
                if (boardBlocks[x, y])
                    boardBlocks[x, y].Fade(0.2f, db.defaultColor);
        }
        else
        {
            for (int y = 0; y < BOARD_SIZE; y++)
                if (!boardBlocks[x, y])
                    return;

            DestroyManager.ins.PrepareToDestroy(x, true);
        }
    }
    public void CheckConectedBlock(bool _val)
    {
        if(_val)
        {
            if(listConnectBlock == null)
            {
                listConnectBlock = new List<BlockTile>();
            }
            else
            {
                listConnectBlock.Clear();
            }
            BlockTile[,] b = new BlockTile[BOARD_SIZE, BOARD_SIZE];
            Array.Copy(boardBlocks, b, boardBlocks.Length);

            Block db = InputManager.ins.draggedBlock;
            Vector2Int c = db.GetFirstCoords();
            for (int i = 0; i < db.structure.Length; i++)
            {
                if (db.transform.GetChild(i).name == "Block tile")
                {
                    BlockTile bt = db.transform.GetChild(i).GetComponent<BlockTile>();
                    Vector2Int _coor = new Vector2Int(c.x + db.structure[i].x, c.y + db.structure[i].y);
                    bt.SetCoordinate(_coor);
                    b[_coor.x, _coor.y] = bt;
                }
            }

            listConnectBlock = GetConnectedBlocks(db.BlockTitles, b);
        }
        else
        {
            if (listConnectBlock != null && listConnectBlock.Count > 0)
            {
                foreach (var _block in listConnectBlock)
                {
                    DestroyManager.ins.PrepareToDestroy(_block.Coordinate.x, _block.Coordinate.y);
                }
            }  
        }
    }
    public List<BlockTile> GetConnectedBlocks(BlockTile[] listCheck, BlockTile[,] shape)
    {
        List<BlockTile> connectedBlocks = new List<BlockTile>();
        List<BlockTile> visitedBlocks = new List<BlockTile>();

        foreach (BlockTile block in listCheck)
        {
            if (!visitedBlocks.Contains(block))
            {
                List<BlockTile> connected = GetConnectedBlocksRecursive(block, shape, visitedBlocks);
                if(connected.Count >=3)
                {
                    connectedBlocks.AddRange(connected);
                }
            }
        }

        return connectedBlocks;
    }
    public List<BlockTile> GetConnectedBlocksRecursive(BlockTile block, BlockTile[,] shape, List<BlockTile> visited)
    {
        List<BlockTile> connected = new List<BlockTile>();
        visited.Add(block);
        connected.Add(block);

        List<BlockTile> neighbors = GetBlockNeighbors(block, shape);

        foreach (BlockTile neighbor in neighbors)
        {
            if (!visited.Contains(neighbor) && neighbor.GetTypeBlock() == block.GetTypeBlock())
            {
                List<BlockTile> connectedNeighbors = GetConnectedBlocksRecursive(neighbor, shape, visited);
                connected.AddRange(connectedNeighbors);
            }
        }

        return connected;
    }
    public List<BlockTile> GetBlockNeighbors(BlockTile block, BlockTile[,] shape)
    {
        List<BlockTile> neighbors = new List<BlockTile>();
        List<Vector2Int> _dirs = new List<Vector2Int>() { new Vector2Int(-1, 0), new Vector2Int(1, 0), new Vector2Int(0, 1), new Vector2Int(0, -1) };

        foreach (var d in _dirs)
        {
            Vector2Int _direction = block.Coordinate + d;
            if (_direction.x >= 0 && _direction.x < BOARD_SIZE && _direction.y >= 0 && _direction.y < BOARD_SIZE)
            {
                BlockTile neighbor = shape[_direction.x, _direction.y];

                if (neighbor != null)
                    neighbors.Add(neighbor);
            }
        }

        return neighbors;
    }

    private bool CheckBlock(int i)
    {
        for (int y = 0; y < BOARD_SIZE; y++)
        {
            for (int x = 0; x < BOARD_SIZE; x++)
            {
                Vector2 size = new Vector2(blocks[i].size.x - 1, blocks[i].size.y - 1);
                Vector2 origin = new Vector2(x, y);
                Vector2 end = origin + size;

                if (IsInRange(origin, end) && IsEmpty(blocks[i], origin))
                    return true;
            }
        }

        return false;
    }
}
