﻿using System;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BlockData
{
    public List<BlockTileData> ListData;
}

public class Block : MonoBehaviour
{
    public BlockTile[] BlockTitles;
    public int prefabIndex;
    public Color defaultColor;
    public Vector2 size;
    public Vector2Int[] structure;
    private Vector2Int[] _rootStructure;

    [HideInInspector]
    public bool movable = true;
    [HideInInspector]
    public int posIndex;
    [HideInInspector]
    public Vector3 basePosition;
    [HideInInspector]
    public Vector3 baseScale;
    [HideInInspector]
    public Vector3 scaledScale;
    private int rotationCount = 0;
    private Vector3 rotate;
    public bool IsRotate { get; private set; }
    public bool IsMoveEnd { get; set; }

    private BlockData data;

    public void Initialized(BlockData _data)
    {
        if(_data != null)
        {
            data = _data;
            for (int i = 0; i < data.ListData.Count; i++)
            {
                BlockTitles[i].SetData(data.ListData[i]);
            }
        }
        _rootStructure = new Vector2Int[structure.Length];
        for (int i = 0; i < structure.Length; i++)
        {
            _rootStructure[i] = structure[i];
        }
    }

    public BlockTile[] GetBlockTitles() => BlockTitles;

    public void SetBasePosition(int i, bool cp = true)
    {
        Vector2 scale = transform.localScale;
        Vector2 colliderSize = GetComponent<BoxCollider>().size * scale;

        Vector3 position = new Vector3(colliderSize.x / 2 - 0.5f + colliderSize.x * i, GameScaler.GetBlockY(), 0);

        basePosition = position;

        if (cp)
            transform.position = basePosition;

        posIndex = i;
    }

    public void Move(float t, Vector3 d)
    {
        GetComponent<BlockMovingAnimation>().enabled = true;
        GetComponent<BlockMovingAnimation>().SetAnimation(t, d);
    }

    public bool IsMoving()
    {
        return GetComponent<BlockMovingAnimation>().enabled;
    }

    public void Scale(bool s, float t)
    {
        GetComponent<BlockScaleAnimation>().enabled = true;
        GetComponent<BlockScaleAnimation>().SetAnimation(s, t);
    }

    public bool IsScaling()
    {
        return GetComponent<BlockScaleAnimation>().enabled;
    }

    public Vector2Int GetFirstCoords()
    {
        Vector3 p;
        p = transform.GetChild(0).transform.position;
        return new Vector2Int((int)(p.x + 0.5f), (int)(p.y + 0.5f));
    }

    public Color GetColor()
    {
        if (transform.GetChild(0).name == "Block tile")
            return transform.GetChild(0).GetComponent<SpriteRenderer>().color;

        return transform.GetChild(1).GetComponent<SpriteRenderer>().color;
    }

    public void ChangeColor(Color c)
    {
        foreach (var t in BlockTitles)
        {
            t.SetColor(c);
        }
    }

    public void Show()
    {
        foreach (var t in BlockTitles)
        {
            if(!t.IsDestroy)
            {
                t.Show();
            }
        }
    }

    public void ScaleTiles(Vector3 s)
    {
        foreach (Transform t in transform)
            t.localScale = s;
    }

    private void Awake()
    {
        baseScale = BoardManager.Instance.boardTileScale;
        scaledScale = BoardManager.Instance.scaledBlockTileScale;

        ScaleTiles(scaledScale);
    }

    internal void Rotate()
    {
        if(GameManager.ins.GetRotateAmount() <= 0)
        {
            return;
        }
        rotationCount++;
        rotate = transform.rotation.eulerAngles;
        rotate.z = rotationCount * 90;
        Debug.Log($"Hieu] Rotate {rotate}");
        transform.DORotate(rotate, 0.2f, RotateMode.FastBeyond360);
        if (rotationCount >= 4)
        {
            rotationCount = 0;           
        }
        
        if(rotationCount == 0 || rotationCount % 4 == 0)
        {
            IsRotate = false;
        }
        else
        {
            IsRotate = true;
        }
        ChangeStructure();
    }

    public void ChangeStructure()
    {
        switch (rotationCount)
        {
            case 0:
                for (int i = 0; i < _rootStructure.Length; i++)
                {
                    structure[i] = _rootStructure[i];
                }
                break;
            case 1:
                ChangeStructure90();
                break;
            case 2:
                ChangeStructure180();
                break;
            case 3:
                ChangeStructure270();
                break;
        }
        var _xSize = size.x;
        var _ySize = size.y;
        size = new Vector2(_ySize, _xSize);
    }

    private void ChangeStructure90()
    {
        for (int i = 0; i < _rootStructure.Length; i++)       
        {
            var d = _rootStructure[i];
            int _x = d.x;
            int _y = d.y;
            if(_x * _y == 0)
            {
                if(_x != 0)
                {
                    structure[i] = new Vector2Int(_y, _x);
                }else if(_y != 0)
                {
                    structure[i] = new Vector2Int(-_y, _x);
                }
                
            }
            else if (_x *_y > 0)
            {
                structure[i] = new Vector2Int(-_y, _x);
            }
            else if (_x * _y < 0)
            {
                structure[i] = new Vector2Int(_y, -_x);
            }           
        }
    }
    private void ChangeStructure180()
    {
        for (int i = 0; i < _rootStructure.Length; i++)
        {
            var d = _rootStructure[i];
            int _x = d.x;
            int _y = d.y;
            structure[i] = new Vector2Int(-_x,- _y);
        }
    }
    private void ChangeStructure270()
    {
        for (int i = 0; i < _rootStructure.Length; i++)
        {
            var d = _rootStructure[i];
            int _x = d.x;
            int _y = d.y;
            if (_x * _y == 0)
            {
                if (_x != 0)
                {
                    structure[i] = new Vector2Int(_y, -_x);
                }
                else if (_y != 0)
                {
                    structure[i] = new Vector2Int(-_y, _x);
                }

            }
            else if (_x *_y > 0)
            {
                structure[i] = new Vector2Int(_x, -_y);
            }
            else if (_x * _y < 0)
            {
                structure[i] = new Vector2Int(-_x, _y);
            }          
        }
    }
}

