using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class UserData
{
    public int BestScore;
    public int CurrentScore;
    public int Match;
    public int Level;
    public int LevelClaimReward;

    public int Rotate { get; internal set; }
}

public class GameSaveData
{
    public static GameSaveData Instance;
    private UserData userData;
    public UserData UserData => userData;
    public GameSaveData()
    {
        Instance = this;
        LoadSave();
    }

    private void LoadSave()
    {
        if (!File.Exists(Application.persistentDataPath + "/myData.json"))
        {
            userData = new UserData();
            SaveUserData();
            return;
        }
        string jsonString = File.ReadAllText(Application.persistentDataPath + "/myData.json");
        userData = JsonConvert.DeserializeObject<UserData>(jsonString);
        Debug.Log("LOAD USER DATA: " + jsonString);
    }

    public void SaveUserData()
    {
        // Save data local
        string json = JsonConvert.SerializeObject(userData);
        File.WriteAllText(Application.persistentDataPath + "/myData.json", json);
        Debug.Log("SAVE USER DATA");
    }

    internal int GetRotate()
    {
        return UserData.Rotate;
    }

    internal int GetScore()
    {
        return UserData.CurrentScore;
    }

    public void SetBestScore(int val)
    {
        if(val >= 0 && val > UserData.BestScore)
        {
            UserData.BestScore = val;
            SaveUserData();
        }
    }

    public int GetBestScore()
    {
        return UserData.BestScore;
    }

    public void UpdateScore(int _score)
    {
        userData.CurrentScore = _score;
        if(userData.BestScore < _score)
        {
            SetBestScore(_score);
        }
        SaveUserData();
    }

    public void UpdateMatch()
    {
        UserData.Match ++;
        UpdateLevel();
        SaveUserData();
    }
    public int GetMatch()
    {
        return UserData.Match;
    }

    internal void SubRotate()
    {
        if(UserData.Rotate > 0)
        {
            UserData.Rotate--;
            SaveUserData();
            GameManager.ins.UpdateRotate();
        }
    }

    internal void SetRotate(int v)
    {
        UserData.Rotate = v;
        SaveUserData();
        GameManager.ins.UpdateRotate();
    }

    public void UpdateLevel()
    {
        int lv = DesignManager.Instance.GetDifficultFromMatch(UserData.Match);
        if(lv > UserData.Level)
        {
            UserData.Level = lv;
            SaveUserData();
        }
    }

    public int GetLevel()
    {
        return UserData.Level;
    }

    public void UpdateLevelReward()
    {
        UserData.LevelClaimReward = UserData.Level;
        SaveUserData();
    }
    public int GetLevelClaimReward()
    {
        return UserData.LevelClaimReward;
    }
}
