using System.IO;
using UnityEditor;
using UnityEngine;

public class BlockPuzzleEditorCustom : MonoBehaviour
{
    [MenuItem("BlockPuzzle/Delete All")]
    public static void DeleteAll()
    {
        PlayerPrefsManager.DeleteAll();
        if (File.Exists(Application.persistentDataPath + "/myData.json"))
        {
            File.Delete(Application.persistentDataPath + "/myData.json");
            PlayerPrefs.DeleteAll();
        }
    }
    [MenuItem("BlockPuzzle/Open PersistentDataPath")]
    public static void OpenPersistentDataPath()
    {
        string path = Application.persistentDataPath;
        EditorUtility.RevealInFinder(path);
    }
}
