using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class LevelDifficult
{
    public int Difficult;
    public Design Data;

    public LevelDifficult Clone()
    {
        return new LevelDifficult()
        {
            Difficult = Difficult,
            Data = Data.Clone(),
        };
    }
}

public class DesignManager:MonoBehaviour
{
    public static DesignManager Instance;
    public List<LevelDifficult> ListData;
    public ColorDefind ColorData;
    public MatchScoreDesign MatchScore;
    public TimeStreak TimeStreak;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }else
        {
            Destroy(gameObject);
        }
    }

    public LevelDifficult GetDesign(int _difficult)
    {
        var _data = ListData.FirstOrDefault((v) => v.Difficult == _difficult);
        if(_data != null)
        {
            return _data.Clone();
        }
        return null;
    }

    public int GetDifficultFromMatch(int _match)
    {
        if(ListData != null)
        {
            for (int i = ListData.Count -1; i >= 0 ; i--)
            {
                if(ListData[i].Data.Matchs < 0)
                {
                    continue;
                }

                if(_match >= ListData[i].Data.Matchs)
                {
                    return ListData[i].Difficult;
                }
            }
        }
        return 0;
    }

    public int GetMatchBeginLevel(int lv)
    {
        if (ListData != null)
        {
            var dt = ListData.FirstOrDefault((v) => v.Difficult == lv);
            if(dt != null)
            {
                return dt.Data.Matchs;
            }
        }
        return 0;
    }
    public int GetDifficultFromPoint(int _score)
    {
        if (ListData != null)
        {
            for (int i = ListData.Count - 1; i >= 0; i--)
            {
                if (ListData[i].Data.PointTarget < 0)
                {
                    continue;
                }

                if (_score >= ListData[i].Data.PointTarget)
                {
                    return ListData[i].Difficult;
                }
            }
        }
        return 0;
    }

    public Color GetColorTypeBlock(int _typeBlock)
    {
        if(ColorData != null)
        {
            var _data = ColorData.ListColor.FirstOrDefault(v => v.TypeBlock == _typeBlock);
            if(_data != null)
            {
                return _data.Color;
            }
        }
        return Color.white;
    }

    public int GetScore(int match)
    {
        if(match > MatchScore.MaxMatch)
        {
            match = MatchScore.MaxMatch;
        }
        var dt = MatchScore.List.FirstOrDefault((v) => v.MathchBlock == match);
        if(dt != null)
        {
            return dt.Point;
        }
        return 0;
    }

    public TimeStreakData GetTimeStreak(float streak)
    {
        if(streak > TimeStreak.MaxStreak)
        {
            streak = TimeStreak.MaxStreak;
        }

        var dt = TimeStreak.List.FirstOrDefault((v) => v.Streak == streak);
        if(dt != null)
        {
            return dt.Clone();
        }
        return null;
    }
}
