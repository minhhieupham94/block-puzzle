﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Collections;

public class GameManager : MonoBehaviour
{
    public static GameManager ins;

    public TextMeshProUGUI fps;
    public TextMeshProUGUI bestScoreText;
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI gameOverBestScoreText;
    public TextMeshProUGUI gameOverScoreText;
    public GameObject scoreLayer;
    public GameObject bestScoreIconLayer;
    public Image offlineLabel;
    public Button continueButton;
    public GameObject addedPointsShell;

    [HideInInspector]
    public bool gameOver = false;
    [HideInInspector]
    public bool paused = false;
    [HideInInspector]
    public bool firstBeatenScore;
    [HideInInspector]
    public bool continueGame;
    [HideInInspector]
    public bool waitingForAd;
    [HideInInspector]
    public int bestScore;

    public int Score { get; set; }
    private Controller controller;
    private GameSaveData saveDataManager;
    private int streak;
    private float timeStreak;
    private int rotateAmount;

    public int GetRotateAmount()
    {
        return rotateAmount + GameSaveData.Instance.GetRotate();
    }

    public void ChangePoints(int e, int combo)
    {
        RectTransform trans = addedPointsShell.GetComponent<RectTransform>();
        trans.anchoredPosition = Camera.main.WorldToScreenPoint(InputManager.ins.lastPosition);
        streak++;
        timeStreak = DesignManager.Instance.GetTimeStreak(streak).Time;
        int points = DesignManager.Instance.GetScore(e) * combo + streak * DesignManager.Instance.GetTimeStreak(streak).Point;

        TextMeshProUGUI t = addedPointsShell.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
        t.text = "+" + points.ToString();

        scoreText.GetComponent<ScoreAddAnimation>().enabled = true;
        scoreText.GetComponent<ScoreAddAnimation>().SetAnimation(points, Score, 0.4f);
        Score += points;
        //saveDataManager.UpdateScore(Score);

        addedPointsShell.GetComponent<Animator>().Play("Fade in");

        ProgressManager.SetBestScore(Score);
    }

    public void SetBestScore()
    {
        bestScoreText.SetText($"{GameSaveData.Instance.GetBestScore()}");
    }

    public void SetScore()
    {
        scoreText.SetText($"{GameSaveData.Instance.GetScore()}");
    }

    public void RestartGame()
    {
        gameOver = false;
        firstBeatenScore = continueGame = true;
        Score = 0;
        scoreText.text = Score.ToString();
        bestScoreIconLayer.GetComponent<Animator>().Play("Idle");

        for (int y = 0; y < BoardManager.BOARD_SIZE; y++)
        {
            for (int x = 0; x < BoardManager.BOARD_SIZE; x++)
            {
                if (BoardManager.Instance.boardBlocks[x, y])
                {
                    Destroy(BoardManager.Instance.boardBlocks[x, y].gameObject);
                    BoardManager.Instance.boardBlocks[x, y] = null;
                }
            }
        }

        for (int i = 0; i < BoardManager.BLOCKS_AMOUNT; i++)
        {
            Destroy(BoardManager.Instance.blocks[i].gameObject);
            int x = Controller.InStance.GetTypeShape();
            BoardManager.Instance.blocks[i] = BoardManager.Instance.SpawnBlock(i, x);
        }
        BoardManager.Instance.AddBlockWaiting(BoardManager.Instance.blocks);
    }

    public void PauseGame()
    {
        paused = true;
        Time.timeScale = 0.0f;
    }

    public void UnpauseGame()
    {
        paused = false;
        Time.timeScale = 1.0f;
    }

    public void SetGameOver()
    {
        gameOver = true;

        SetBestScore();
        gameOverScoreText.text = Score.ToString();
    }

    public void FadeBlocks()
    {
        if (!ScenesManager.ins.transition)
            StartCoroutine(WaitForFade());
    }

    public IEnumerator WaitForFade()
    {
        gameOver = true;

        ScenesManager.ins.transition = true;
        
        for (int y = BoardManager.BOARD_SIZE - 1; y >= 0; y--)
        {
            for (int x = 0; x < BoardManager.BOARD_SIZE; x++)
            {
                BlockTile b = BoardManager.Instance.boardBlocks[x, y];
                if (b)
                    b.Fade(0.25f, new Color(0.09f, 0.122f, 0.153f));

                if (x % 2 == 0)
                   yield return new WaitForSeconds(0.01f);
            }
        }

        yield return new WaitForSeconds(0.25f);
        
        ScenesManager.ins.LoadGameOverScreen();
    }

    public void ContinueGame()
    {
        gameOver = false;
        continueGame = false;

        ChangeBlocksColor();
        ScenesManager.ins.LoadGameFromGameOverScreen();
    }

    public void DestroyBlocks()
    {
        int a = (int)Random.Range(0, BoardManager.BOARD_SIZE - 2.001f);
        if (BoardManager.Instance.blocks[0].size.x >= BoardManager.Instance.blocks[0].size.y)
        {
            for (int y = a; y < a + 3; y++)
            {
                for (int x = 0; x < BoardManager.BOARD_SIZE; x++)
                {
                    BlockTile b = BoardManager.Instance.boardBlocks[x, y];
                    if (b)
                        b.Destroy(0.25f);

                    BoardManager.Instance.boardBlocks[x, y] = null;
                }
            }
        }
        else
        {
            for (int x = a; x < a + 3; x++)
            {
                for (int y = 0; y < BoardManager.BOARD_SIZE; y++)
                {
                    BlockTile b = BoardManager.Instance.boardBlocks[x, y];
                    if (b)
                        b.Destroy(0.25f);

                    BoardManager.Instance.boardBlocks[x, y] = null;
                }
            }
        }
        
        BoardManager.Instance.CheckBoard();
    }

    public void ChangeBlocksColor()
    {
        for (int y = 0; y < BoardManager.BOARD_SIZE; y++)
            for (int x = 0; x < BoardManager.BOARD_SIZE; x++)
                if (BoardManager.Instance.boardBlocks[x, y])
                    BoardManager.Instance.boardBlocks[x, y].Fade(0.0f, BoardManager.Instance.boardBlocks[x, y].defaultColor);
    }

    private void Awake()
	{
        if (!ins)
            ins = this;

        Application.targetFrameRate = 60;
    }

    private void Start()
    {
        controller = new Controller();
        saveDataManager = new GameSaveData();
        bestScore = ProgressManager.GetBestScore();
        BoardManager.Instance.Initialized();
    }

    private void Update()
    {
        fps.text = ((int)(1f / Time.smoothDeltaTime)).ToString();

        if(timeStreak > 0)
        {
            timeStreak -= Time.deltaTime;
            if(timeStreak <= 0)
            {
                timeStreak = 0;
                streak = 0;
            }
        }
    }

    internal void SubRotate()
    {
        if(rotateAmount > 0)
        {
            rotateAmount--;
        }
        else
        {
            GameSaveData.Instance.SubRotate();
        }
    }

    public void ResetRotateBeginGame()
    {
        rotateAmount = 5;
    }

    public void UpdateRotate(bool issub = false)
    {
        if(!issub)
        {
            GUIInGame.Instance.SetTextButton(GetRotateAmount());
        }
        else
        {
            GUIInGame.Instance.SetTextButton(GetRotateAmount() - 1) ;
        }
    }
}
