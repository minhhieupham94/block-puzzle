﻿using UnityEngine;
using System.Collections;

public class ProgressManager : MonoBehaviour
{
    public static void LoadProgress()
    {
        bool[] audioMutes = GetAudioMutes();
        float[] audioVolumes = GetAudioVolumes();

        AudioManager.ins.ChangeMusicVolume(audioVolumes[0]);
        AudioManager.ins.ChangeSoundVolume(audioVolumes[1]);
        AudioManager.ins.ChangeSliderValues(audioVolumes);

        if (audioMutes[0])
            AudioManager.ins.MuteMusic();
        else
            AudioManager.ins.UnmuteMusic();
        if (audioMutes[1])
            AudioManager.ins.MuteSound();
        else
            AudioManager.ins.UnmuteSound();

        GameManager.ins.firstBeatenScore = PlayerPrefsManager.HasKey(PlayerPrefsKey.FIRST_BEATON_SCORE) ? PlayerPrefsManager.GetBool(PlayerPrefsKey.FIRST_BEATON_SCORE) : true;
        GameManager.ins.continueGame = PlayerPrefsManager.HasKey(PlayerPrefsKey.CONTINUE_GAME) ? PlayerPrefsManager.GetBool(PlayerPrefsKey.CONTINUE_GAME) : true;
        GameManager.ins.SetScore();
        GameManager.ins.SetBestScore();

        if (!GameManager.ins.firstBeatenScore)
            GameManager.ins.bestScoreIconLayer.GetComponent<Animator>().Play("Beaten score");

        for (int y = 0; y < BoardManager.BOARD_SIZE; y++)
        {
            for (int x = 0; x < BoardManager.BOARD_SIZE; x++)
            {
                Color color = GetColor(GetBlockKey(x, y));

                if (color != Color.black)
                {
                    BlockTile b = BoardManager.Instance.SpawnBlockTile(x, y);
                    //b.GetComponent<SpriteRenderer>().color = color;
                   // b.defaultColor = color;
                }
            }
        }

        for (int i = 0; i < BoardManager.BLOCKS_AMOUNT; i++)
        {
            if (PlayerPrefsManager.HasKey(PlayerPrefsKey.Block(i)))
            {
                int prefabIndex = PlayerPrefsManager.GetInt(PlayerPrefsKey.Block(i));
                BoardManager.Instance.blocks[i] = BoardManager.Instance.SpawnBlock(i, prefabIndex);
            }
        }

        if(BoardManager.Instance.blocks != null && BoardManager.Instance.blocks.Length > 0)
        {
            BoardManager.Instance.AddBlockWaiting(BoardManager.Instance.blocks);
        }
    }

    public static void SaveProgress()
    {
        SetAudioMutes(AudioManager.ins.GetAudioMutes());
        SetAudioVolumes(AudioManager.ins.GetAudioVolumes());
        SetBestScore(GameManager.ins.bestScore);

        // // // // // // // // //      SAVE BOARD PROGRESS (IS NOT GAME OVER)      // // // // // // // // //
        if (!GameManager.ins.gameOver)
        {
            PlayerPrefsManager. SetBool(PlayerPrefsKey.FIRST_BEATON_SCORE, GameManager.ins.firstBeatenScore);
            PlayerPrefsManager.SetBool(PlayerPrefsKey.CONTINUE_GAME, GameManager.ins.continueGame);
            GameSaveData.Instance.UpdateScore(GameManager.ins.Score);

            for (int y = 0; y < BoardManager.BOARD_SIZE; y++)
            {
                for (int x = 0; x < BoardManager.BOARD_SIZE; x++)
                {
                    if (BoardManager.Instance.boardBlocks[x, y])
                    {
                        Color color = BoardManager.Instance.boardBlocks[x, y].defaultColor;
                        SetColor(GetBlockKey(x, y), color);
                    }
                    else
                    {
                        SetColor(GetBlockKey(x, y), Color.black);
                    }
                }
            }

            for (int i = 0; i < BoardManager.BLOCKS_AMOUNT; i++)
                PlayerPrefsManager.SetInt(PlayerPrefsKey.Block(i), BoardManager.Instance.blocks[i].prefabIndex);
        }
        // // // // // // // // //      DO NOT SAVE BOARD PROGRESS (IS GAME OVER)      // // // // // // // // //
        else
        {
            PlayerPrefsManager.SetBool(PlayerPrefsKey.FIRST_BEATON_SCORE, true);
            PlayerPrefsManager.SetBool(PlayerPrefsKey.CONTINUE_GAME, true);
            GameSaveData.Instance.UpdateScore(0);

            for (int y = 0; y < BoardManager.BOARD_SIZE; y++)
                for (int x = 0; x < BoardManager.BOARD_SIZE; x++)
                        SetColor(GetBlockKey(x, y), Color.black);

            for (int i = 0; i < BoardManager.BLOCKS_AMOUNT; i++)
                PlayerPrefsManager.SetInt(PlayerPrefsKey.Block(i), BoardManager.Rand(0, BoardManager.BLOCK_PREFABS_AMOUNT));
        }
    }

    public static bool[] GetAudioMutes()
    {
        bool[] am = new bool[2];

        am[0] = PlayerPrefsManager.GetBool(PlayerPrefsKey.MUSIC);
        am[1] = PlayerPrefsManager.GetBool(PlayerPrefsKey.SOUND);

        return am;
    }

    public static float[] GetAudioVolumes()
    {
        float[] av = new float[2];

        av[0] = PlayerPrefsManager.HasKey(PlayerPrefsKey.MUSIC_VOL) ? PlayerPrefsManager.GetFloat(PlayerPrefsKey.MUSIC_VOL) : 0.4f;
        av[1] = PlayerPrefsManager.HasKey(PlayerPrefsKey.SOUND_VOL) ? PlayerPrefsManager.GetFloat(PlayerPrefsKey.SOUND_VOL) : 1.0f;

        return av;
    }

    public static void SetAudioMutes(bool[] v)
    {
        PlayerPrefsManager. SetBool(PlayerPrefsKey.MUSIC_VOL, v[0]);
        PlayerPrefsManager.SetBool(PlayerPrefsKey.SOUND_VOL, v[1]);
    }

    public static void SetAudioVolumes(float[] v)
    {
        PlayerPrefsManager.SetFloat(PlayerPrefsKey.MUSIC, v[0]);
        PlayerPrefsManager.SetFloat(PlayerPrefsKey.SOUND, v[1]);
    }

    public static int GetBestScore()
    {
        return GameSaveData.Instance.GetBestScore();
    }

    public static void SetBestScore(int s = 0)
    {
        int bs = GameManager.ins.bestScore;

        if (s > bs)
        {
            GameManager.ins.bestScoreText.GetComponent<ScoreAddAnimation>().enabled = true;
            GameManager.ins.bestScoreText.GetComponent<ScoreAddAnimation>().SetAnimation(s - bs, bs, 0.4f);
            GameManager.ins.bestScore = s;
            GameSaveData.Instance.SetBestScore(s);

            if (GameManager.ins.firstBeatenScore)
            {
                GameManager.ins.bestScoreIconLayer.GetComponent<Animator>().Play("Beaten score");
                GameManager.ins.firstBeatenScore = false;
            }
        }
    }

    private void Awake()
	{
        StartCoroutine(Wait());
	}

    private IEnumerator Wait()
    {
        yield return new WaitForSeconds(0.02f);

        LoadProgress();
        BoardManager.Instance.CheckBoard(true);
    }

    private void OnApplicationPause(bool isPaused)
    {
        if (isPaused)
            SaveProgress();
    }

    private static string GetBlockKey(int x, int y)
    {
        return "block[" + x + ", " + y;
    }

    private static Color GetColor(string k)
    {
        float[] c = PlayerPrefsManager. GetFloatArray(k, 4);

        if (c == null)
            return Color.black;

        return new Color(c[0], c[1], c[2], c[3]);
    }

    private static void SetColor(string k, Color c)
    {
        float[] comp = new float[] { c.r, c.g, c.b, c.a };
        PlayerPrefsManager. SetFloatArray(k, comp);
    }

}
