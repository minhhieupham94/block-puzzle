﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;

public class InputManager : MonoBehaviour
{
    public static InputManager ins;

    [HideInInspector]
    public Vector3 lastPosition;
    [HideInInspector]
    public Block draggedBlock;

    private ScreenOrientation screenOrientation;

    private Vector3 startPos;
    private bool isDragging;
    private Vector2Int lastPos = new Vector2Int(-1, -1);
    private SpriteRenderer[] highlightedTiles = new SpriteRenderer[9];

    public void ResetBlock()
    {
        if (draggedBlock)
        {
            MoveDraggedBlock();
            ResetDraggedBlock();
        }
    }

	private void Awake()
	{
        if (!ins)
            ins = this;
    }

	private void Update()
	{
        if (screenOrientation != Screen.orientation || GameManager.ins.paused)
            ResetBlock();

        if (Input.touchPressureSupported)
        {
            if (Input.touchCount > 0)
            {
                Touch t = Input.GetTouch(0);
                if (t.phase == TouchPhase.Began && !GameManager.ins.paused)
                {
                    OnbeginDrag(t.position);
                }
                else if (t.phase == TouchPhase.Moved && draggedBlock)
                {
                    OnDrag(t.position);
                }
                // // // // // // // // //      ENDED      // // // // // // // // //
                else if (t.phase == TouchPhase.Ended && draggedBlock)
                {
                    OnEndDrag();
                }
            }
        }
        else
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (!GameManager.ins.paused && !isDragging)
                {
                    OnbeginDrag(Input.mousePosition);
                }
            }
            if (draggedBlock && isDragging == true)
            {
                OnDrag(Input.mousePosition);
            }
            if (Input.GetMouseButtonUp(0))
            {
                OnEndDrag();
            }
        }
    }

    private void OnbeginDrag(Vector3 _position)
    {
        Ray ray = Camera.main.ScreenPointToRay(_position);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 15))
        {
            screenOrientation = Screen.orientation;

            Collider c = hit.collider;

            if (c.tag == "Block" && c.GetComponent<Block>().movable && !c.GetComponent<Block>().IsMoving())
            {
                draggedBlock = c.GetComponent<Block>();

                draggedBlock.Scale(true, 0.2f);

                Color cl = draggedBlock.defaultColor; cl.a = 0.66f;
                draggedBlock.ChangeColor(cl);

                Vector3 p = draggedBlock.transform.position;
                startPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                startPos = new Vector3(startPos.x - p.x, startPos.y - p.y, 0);
                isDragging = true;
            }
        }
    }

    private void OnDrag(Vector3 _position)
    {
        if (draggedBlock && isDragging == true)
        {
            Vector3 pos = Camera.main.ScreenToWorldPoint(_position);
            pos = new Vector3(pos.x, pos.y, -2);

            draggedBlock.transform.position = pos - startPos;

            Vector3 size = Vector3.zero;
            foreach (var str in draggedBlock.structure)
            {
                if(str.x > size.x)
                {
                    size.x = str.x;
                }

                if (str.y > size.y)
                {
                    size.y = str.y;
                }
            }

            Vector2 origin = draggedBlock.transform.GetChild(0).position;
            Vector2 end = draggedBlock.transform.GetChild(0).position + size;

            if (IsInRange(origin, end) && IsEmpty(draggedBlock, RoundVector2(origin)))
            {
                Vector2Int start = RoundVector2(origin);

                if (lastPos != start)
                {
                    RemoveAllHighlights();
                    BoardManager.Instance.HighlightBlocks();

                    for (int i = 0; i < draggedBlock.structure.Length; i++)
                    {
                        if (draggedBlock.transform.GetChild(i).name == "Block tile")
                        {
                            Vector2Int coords = draggedBlock.structure[i];
                            highlightedTiles[i] = BoardManager.Instance.boardTiles[start.x + coords.x, start.y + coords.y];
                            highlightedTiles[i].color = BoardManager.Instance.highlightColor;
                        }
                    }
                }

                lastPos = start;
            }
            else
            {
                RemoveAllHighlights();
                lastPos = new Vector2Int(-1, -1);
            }
        }
    }

    private void OnEndDrag()
    {
        if (draggedBlock && isDragging)
        {
            Vector3 size = Vector3.zero;
            foreach (var str in draggedBlock.structure)
            {
                if (str.x > size.x)
                {
                    size.x = str.x;
                }

                if (str.y > size.y)
                {
                    size.y = str.y;
                }
            }

            Vector2 origin = draggedBlock.transform.GetChild(0).position;
            Vector2 end = draggedBlock.transform.GetChild(0).position + size;

            // // // // // // // // //  CAN PUT ON BOARD  // // // // // // // // //
            if (IsInRange(origin, end) && IsEmpty(draggedBlock, RoundVector2(origin)))
            {
                Vector2Int start = RoundVector2(origin);
                for (int i = 0; i < draggedBlock.structure.Length; i++)
                {
                    var _blockTitle = draggedBlock.transform.GetChild(i);
                    if (_blockTitle.name == "Block tile")
                    {
                        Vector2Int coords = draggedBlock.structure[i];
                        highlightedTiles[i] = BoardManager.Instance.boardTiles[start.x + coords.x, start.y + coords.y];
                        BlockTile b = _blockTitle.GetComponent<BlockTile>();
                        BoardManager.Instance.boardBlocks[start.x + coords.x, start.y + coords.y] = b;
                        var _target = BoardManager.Instance.boardTiles[start.x + coords.x, start.y + coords.y].transform.position;
                        _target.z = -1;
                        _blockTitle.DOMove(_target, 0.08f);
                    }
                }
                draggedBlock.ChangeColor(draggedBlock.defaultColor);
                draggedBlock.GetComponent<BoxCollider>().enabled = false;
                draggedBlock.enabled = false;

                AudioManager.ins.PlayBlockSound();

                BoardManager.Instance.MoveBlocks(draggedBlock.posIndex);
                BoardManager.Instance.CheckBoard();
                if(draggedBlock.IsRotate)
                {
                    GameManager.ins.SubRotate();
                }
                draggedBlock.IsMoveEnd = true;
            }
            // // // // // // // // // CANNOT PUT ON BOARD // // // // // // // // //
            else
            {
                MoveDraggedBlock();
            }

            ResetDraggedBlock();
            isDragging = false;
        }

    }

    private Vector2Int RoundVector2(Vector2 v)
    {
        return new Vector2Int((int)(v.x + 0.5f), (int)(v.y + 0.5f));
    }

    private Vector3 BlockPosition(Vector2 o, Vector2 s)
    {
        Vector3 off = Vector3.zero;

        if (s.x % 2 == 1)
            off.x = 0.5f;
        if (s.y % 2 == 1)
            off.y = 0.5f;

        return new Vector3((int)(o.x + 0.5f) + (int)(s.x / 2), (int)(o.y + 0.5f) + (int)(s.y / 2), -1) + off;
    }

    private bool IsInRange(Vector2 o, Vector2 e)
    {
        return BoardManager.Instance.IsInRange(o, e);
    }

    private bool IsEmpty(Block b, Vector2 o)
    {
        return BoardManager.Instance.IsEmpty(b, o);
    }

    private void RemoveAllHighlights()
    {
        if (!GameManager.ins.gameOver)
        {
            foreach (BlockTile b in BoardManager.Instance.boardBlocks)
            {
                if (b)
                {
                    b.Fade(0.2f, b.defaultColor);
                }

            }
                
        }
            

        for (int i = 0; i < 9; i++)
        {
            if (highlightedTiles[i])
            {
                highlightedTiles[i].color = BoardManager.Instance.boardColor;
                highlightedTiles[i] = null;
            }
        }
    }
	
	private void OnApplicationPause(bool isPaused)
	{
        if (isPaused)
            ResetBlock();
	}
	
    private void MoveDraggedBlock()
    {
        draggedBlock.Scale(false, 0.2f);
        draggedBlock.Move(0.25f, draggedBlock.basePosition);
        draggedBlock.ChangeColor(draggedBlock.defaultColor);
    }

    private void ResetDraggedBlock()
    {
        startPos = Vector3.zero;
        draggedBlock = null;
        RemoveAllHighlights();
    }
}
