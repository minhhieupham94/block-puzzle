using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderCustom : MonoBehaviour
{
    private Image sliderImg;
    private Vector2 rootSize;
    public RectTransform rctBg;
    public RectTransform rct;
    private float curval;
    private void Awake()
    {
        sliderImg = GetComponent<Image>();
        rootSize = rctBg.rect.size;
        rct.sizeDelta = new Vector2(0, rootSize.y);
    }

    public void SetSlider(float per)
    {
        if(per > curval)
        {
            rct.DOSizeDelta(new Vector2(per * rootSize.x, rootSize.y), 0.2f);
        }
    }
}
