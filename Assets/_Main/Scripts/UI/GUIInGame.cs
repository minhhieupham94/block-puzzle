using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GUIInGame : MonoBehaviour
{
    public static GUIInGame Instance;
    public TextMeshProUGUI[] TextButtoms;
    public SliderCustom Slider;
    private void Awake()
    {
        Instance = this;
    }
    public void OnClickRotate(int i)
    {
        BoardManager.Instance.RotateBlock(i);
    }

    public bool IsClaimReward()
    {
        if(GameSaveData.Instance.GetLevelClaimReward() < GameSaveData.Instance.GetLevel())
        {
            return true;        
        }
        return false;
    }

    public void ClaimReward()
    {
        if(IsClaimReward())
        {
            int index = GameSaveData.Instance.GetLevel() - GameSaveData.Instance.GetLevelClaimReward();
            GameSaveData.Instance.SetRotate(index * 5);
            GameSaveData.Instance.UpdateLevelReward();
        }
    }

    public void SetTextButton(int v)
    {
        if(TextButtoms != null)
        {
            foreach (var t in TextButtoms)
            {
                t.SetText($"Rotate ({v})");
            }
        }
    }

    public void UpdateSlider()
    {
        int begin = DesignManager.Instance.GetMatchBeginLevel(GameSaveData.Instance.GetLevelClaimReward());
        int newLv = DesignManager.Instance.GetMatchBeginLevel(GameSaveData.Instance.GetLevelClaimReward() + 1);
        int total = newLv - begin;

        float per = (GameSaveData.Instance.GetMatch() - begin) / total;
        Slider.SetSlider(per);
    }
}
